const _ = require('lodash')

const userSanitized = require('../utils').userSanitized
const formatError = require('../utils').formatError

const distance = (lat1, lon1, lat2, lon2) => {
  const { PI, cos, sin, atan2, sqrt, pow } = Math

  const chordDist = pow(sin(((lat2 - lat1) * PI / 180) / 2), 2) +
    cos(lat1 * PI / 180) * cos(lat2 * PI / 180) * pow(sin(((lon2 - lon1) * PI / 180) / 2), 2)

  const angularDistRad = 2 * atan2(sqrt(chordDist), sqrt(1 - chordDist))

  return (6371000 * angularDistRad) <= 1000
}

module.exports = {
  test: async (ctx) => {
    const { email } = ctx.request.body

    if (email === undefined) {
      return ctx.response.badRequest({
        id: 'Auth.form.error.invalid',
        message: 'Email is required.',
      })
    }

    const query = {
      provider: 'local',
      email,
    }

    const user = await strapi.query('user', 'users-permissions')
                             .findOne(query)

    if (user === null) {
      return ctx.response.status = 204
    }

    return ctx.response.conflict({
      id: 'Auth.form.error.email.taken',
      message: 'Email is already taken.',
    })
  },

  neighbors: async (ctx) => {
    const { id } = await strapi.plugins['users-permissions'].services.jwt.getToken(ctx)

    let { lat, lng } = ctx.query
    if (lat === undefined || lng === undefined) {
      return ctx.response.badRequest({
        id: 'Neighbors.error.latLng.required',
        message: 'Latitude & longitude are required.',
      })
    }

    lat = parseFloat(lat)
    lng = parseFloat(lng)

    if (isNaN(lat) || isNaN(lng)) {
      return ctx.response.badRequest({
        id: 'Neighbors.error.latLng.invalid',
        message: 'Latitude & longitude have to be number.',
      })
    }

    const query = {
      provider: 'local',
      id,
    }
    const me = await strapi.query('user', 'users-permissions')
                           .findOne(query)
    const users = await strapi.plugins['users-permissions'].services.user.fetchAll()

    ctx.body = users
      .filter(user => {
        if (user.id === me.id || !user.locationShared) return false

        return user.location.some(({ lat: userLat, lng: userLng }) => distance(lat, lng, userLat, userLng))
      })
      .map(userSanitized)
  },

  find: async (ctx, next, { populate } = {}) => {
    let users

    if (_.has(ctx.query, '_q')) {
      // use core strapi query to search for users
      users = await strapi.query('user', 'users-permissions')
                          .search(ctx.query, populate)
    } else {
      users = await strapi.plugins['users-permissions'].services.user.fetchAll(ctx.query, populate)
    }

    ctx.body = users.map(userSanitized)
  },

  create: async (ctx) => {
    const advanced = await strapi
      .store({
        environment: '',
        type: 'plugin',
        name: 'users-permissions',
        key: 'advanced',
      })
      .get()

    const {
      email,
      password,
      role,
      gender,
      birthdayYear,
      productionActivity,
      productionShared,
      locationShared,
      generalTermsOfUse,
    } = ctx.request.body

    if (!email) return ctx.badRequest('missing.email')
    if (!password) return ctx.badRequest('missing.password')
    if (!gender) return ctx.badRequest('missing.gender')
    if (!birthdayYear) return ctx.badRequest('missing.birthdayYear')
    if (!productionActivity) return ctx.badRequest('missing.productionActivity')
    if (productionShared === undefined) return ctx.badRequest('missing.productionShared')
    if (locationShared === undefined) return ctx.badRequest('missing.locationShared')
    if (!generalTermsOfUse) return ctx.badRequest('missing.generalTermsOfUse')

    const userWithSameEmail = await strapi
      .query('user', 'users-permissions')
      .findOne({ email: email.toLowerCase() })

    if (userWithSameEmail) {
      return ctx.badRequest(
        null,

        formatError({
          id: 'Auth.form.error.email.taken',
          message: 'Email already taken.',
          field: ['email'],
        }),
      )
    }

    const user = {
      ...ctx.request.body,
      provider: 'local',
      username: email,
    }

    user.email = user.email.toLowerCase()

    if (!role) {
      const defaultRole = await strapi
        .query('role', 'users-permissions')
        .findOne({ type: advanced.default_role }, [])

      user.role = defaultRole.id
    }

    try {
      const locations = []
      for (const location of user.locations) {
        const locationCreated = await strapi.services.location.create(location)
        locations.push(locationCreated.id)
      }
      user.locations = locations
      const data = await strapi.plugins['users-permissions'].services.user.add(user)

      ctx.created(userSanitized(data))
    } catch (error) {
      ctx.badRequest(null, formatError(error))
    }
  },

  callback: async (ctx) => {
    const store = await strapi.store({
      environment: '',
      type: 'plugin',
      name: 'users-permissions',
    })

    if (!_.get(await store.get({ key: 'grant' }), 'email.enabled')) {
      return ctx.badRequest(null, 'This provider is disabled.')
    }

    // The identifier is required.
    if (!params.identifier) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.email.provide',
          message: 'Please provide your username or your e-mail.',
        }),
      )
    }

    // The password is required.
    if (!params.password) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.password.provide',
          message: 'Please provide your password.',
        }),
      )
    }

    const query = {
      provider: 'local',
      email: params.identifier.toLowerCase(),
    }

    // Check if the user exists.
    const user = await strapi.query('user', 'users-permissions')
                             .findOne(query)

    if (!user) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.invalid',
          message: 'Identifier or password invalid.',
        }),
      )
    }

    if (
      _.get(await store.get({ key: 'advanced' }), 'email_confirmation') &&
      user.confirmed !== true
    ) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.confirmed',
          message: 'Your account email is not confirmed',
        }),
      )
    }

    if (user.blocked === true) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.blocked',
          message: 'Your account has been blocked by an administrator',
        }),
      )
    }

    const validPassword = await strapi.plugins[
      'users-permissions'
      ].services.user.validatePassword(params.password, user.password)

    if (!validPassword) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.invalid',
          message: 'Identifier or password invalid.',
        }),
      )
    } else {
      ctx.send({
        jwt: strapi.plugins['users-permissions'].services.jwt.issue({
          id: user.id,
        }),
        user: userSanitized(user),
      })
    }
  },

  async me(ctx) {
    const user = ctx.state.user;

    if (!user) {
      return ctx.badRequest(null, [{ messages: [{ id: 'No authorization header was found' }] }]);
    }

    const locations = await strapi.services.location.find({user: user.id})

    ctx.body = userSanitized(user, locations);
  },
}
