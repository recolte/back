const _ = require('lodash')

const userSanitized = require('../utils').userSanitized
const formatError = require('../utils').formatError

module.exports = {
  callback: async (ctx) => {
    const params = ctx.request.body;
    const store = await strapi.store({
      environment: '',
      type: 'plugin',
      name: 'users-permissions',
    })

    if (!_.get(await store.get({ key: 'grant' }), 'email.enabled')) {
      return ctx.badRequest(null, 'This provider is disabled.')
    }

    // The identifier is required.
    if (!params.identifier) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.email.provide',
          message: 'Please provide your username or your e-mail.',
        }),
      )
    }

    // The password is required.
    if (!params.password) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.password.provide',
          message: 'Please provide your password.',
        }),
      )
    }

    const query = {
      provider: 'local',
      email: params.identifier.toLowerCase(),
    }

    // Check if the user exists.
    const user = await strapi.query('user', 'users-permissions')
                             .findOne(query)

    if (!user) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.invalid',
          message: 'Identifier or password invalid.',
        }),
      )
    }

    if (
      _.get(await store.get({ key: 'advanced' }), 'email_confirmation') &&
      user.confirmed !== true
    ) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.confirmed',
          message: 'Your account email is not confirmed',
        }),
      )
    }

    if (user.blocked === true) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.blocked',
          message: 'Your account has been blocked by an administrator',
        }),
      )
    }

    const validPassword = await strapi.plugins[
      'users-permissions'
      ].services.user.validatePassword(params.password, user.password)

    if (!validPassword) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.invalid',
          message: 'Identifier or password invalid.',
        }),
      )
    } else {
      ctx.send({
        jwt: strapi.plugins['users-permissions'].services.jwt.issue({
          id: user.id,
        }),
        user: userSanitized(user),
      })
    }
  },
}
