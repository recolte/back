module.exports = {
  userSanitized: (user, locations) => {
    const {
      id,
      email,
      confirmed,
      blocked,
      role,
      created_at,
      updated_at,
      gender,
      birthdayYear,
      job,
      productionActivity,
      locationType,
      surfaceNumber,
      surfaceUnity,
      productionType,
      productionTypeNumber,
      productionShared,
      locationShared,
    } = user

    const { id: roleId, name, type } = role
    const userReturned = {
      id,
      email,
      confirmed,
      blocked,
      role: { id: roleId, name, type },
      createdAt: created_at,
      updatedAt: updated_at,
      gender,
      productionActivity,
    }

    if (birthdayYear) userReturned.birthdayYear = birthdayYear
    if (job) userReturned.job = job
    if (locationType) userReturned.locationType = locationType
    if (locations) {
      userReturned.locations = locations.map(location => {
        const { id, lat, lng, surface, unit } = location
        return { id, lat, lng, surface: parseInt(surface), unit }
      })
    } else {
      userReturned.locations = []
    }
    if (surfaceNumber) userReturned.surfaceNumber = surfaceNumber
    if (surfaceUnity) userReturned.surfaceUnity = surfaceUnity
    if (productionType) userReturned.productionType = productionType
    if (productionTypeNumber) userReturned.productionTypeNumber = productionTypeNumber
    if (productionShared) userReturned.productionShared = productionShared
    if (locationShared) userReturned.locationShared = locationShared

    return userReturned
  },

  formatError: error => ([
    { messages: [{ id: error.id, message: error.message, field: error.field }] },
  ]),
}
