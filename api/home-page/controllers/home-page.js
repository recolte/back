'use strict'

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async find () {
    const { id, title, description, locale, cards, localizations } = await strapi.services['home-page'].find()

    return {
      id,
      title,
      description,
      locale,
      localizations,
      cards: cards.map(card => {
        const { id: cardId, title, description: descriptionCard, illustration } = card
        const { id: illustrationId, alternativeText, url } = illustration
        return {
          id: cardId,
          title,
          description: descriptionCard,
          illustration: { id: illustrationId, alternativeText, url },
        }
      }),
    }
  },
}
