'use strict'

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const sanitizeVegetable = (vegetable) => {
  const { id, name, locale, picture, localizations } = vegetable
  const { id: pictureId, url, alternativeText } = picture

  return {
    id,
    name,
    locale,
    picture: {
      id: pictureId,
      url,
      alternativeText,
    },
    localizations,
  }
}

module.exports = {
  listUser: async (ctx) => {
    const { id } = await strapi.plugins['users-permissions'].services.jwt.getToken(ctx)

    const harvests = await strapi.services.harvest.find({ user: id })
    const vegetables = new Set()
    harvests.forEach(harvest => vegetables.add(harvest.vegetable.id))

    return { vegetables: [...vegetables] }
  },

  find () {
    return strapi.services.vegetable.find()
                 .then(entities => entities.map(entity => sanitizeVegetable(entity)))
  },
}
