'use strict'

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const sanitizeHarvest = (harvest) => {
  const { id, number, weight, vegetable, user } = harvest
  const { id: vegetableId, name, picture } = vegetable
  const { id: pictureId, url, alternativeText } = picture
  const { id: userId } = user

  return {
    id,
    number: parseInt(number),
    weight: parseInt(weight),
    vegetable: {
      id: vegetableId,
      name,
      picture: {
        id: pictureId,
        url,
        alternativeText,
      },
    },
    user: userId,
  }
}

module.exports = {
  async create (ctx) {
    const { id } = await strapi.plugins['users-permissions'].services.jwt.getToken(ctx)
    const entity = await strapi.services.harvest.create({ ...ctx.request.body, user: id })
    ctx.response.status = 201
    return sanitizeHarvest(entity)
  },

  async find (ctx) {
    const { id } = await strapi.plugins['users-permissions'].services.jwt.getToken(ctx)
    const entities = await strapi.services.harvest.find({ user: id })

    return entities.map(entity => sanitizeHarvest(entity))
  },

  async findOne (ctx) {
    const { id: userId } = await strapi.plugins['users-permissions'].services.jwt.getToken(ctx)
    const { id } = ctx.params;

    const entity = await strapi.services.harvest.findOne({ id });

    if (entity === null) {
      return ctx.response.notFound()
    }

    if (entity.user.id !== userId) {
      return ctx.response.forbidden()
    }

    return sanitizeHarvest(entity)
  },

  async count(ctx) {
    const { id } = await strapi.plugins['users-permissions'].services.jwt.getToken(ctx)

    return strapi.services.harvest.count({user: id})
  }
}
